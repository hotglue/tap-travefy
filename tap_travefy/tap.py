"""travefy tap class."""

from typing import List

from singer_sdk import Tap, Stream
from singer_sdk import typing as th  # JSON schema typing helpers
# TODO: Import your custom stream types here:
from tap_travefy.streams import (
    travefyStream,
    UsersStream
)
# TODO: Compile a list of custom stream types here
#       OR rewrite discover_streams() below with your custom logic.
STREAM_TYPES = [
    UsersStream
]


class Taptravefy(Tap):
    """travefy tap class."""
    name = "tap-travefy"
    # TODO: Update this section with the actual config values you expect:
    config_jsonschema = th.PropertiesList(
        th.Property(
            "private_key",
            th.StringType,
            required=True,
            description="The private key to authenticate against the API service"
        ),
        th.Property(
            "public_key",
            th.StringType,
            required=True,
            description="The public key to authenticate against the API service"
        ),
        th.Property(
            "sandbox",
            th.BooleanType,
            description="Define if sandbox is TRUE or FAlse. Default is TRUE"
        ),
        th.Property(
            "api_url",
            th.StringType,
            default="https://api.travefy.com",
            description="The url for the API service"
        ),
    ).to_dict()

    def discover_streams(self) -> List[Stream]:
        """Return a list of discovered streams."""
        return [stream_class(tap=self) for stream_class in STREAM_TYPES]

if __name__ == '__main__':
    Taptravefy.cli()     

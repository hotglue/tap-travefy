"""Stream type classes for tap-travefy."""

from pathlib import Path
from typing import Any, Dict, Optional, Union, List, Iterable

from singer_sdk import typing as th  # JSON Schema typing helpers

from tap_travefy.client import travefyStream

# TODO: Delete this is if not using json files for schema definition
SCHEMAS_DIR = Path(__file__).parent / Path("./schemas")
# TODO: - Override `UsersStream` and `GroupsStream` with your own stream definition.
#       - Copy-paste as many times as needed to create multiple stream types.


class UsersStream(travefyStream):
    """Define custom stream."""
    name = "users"
    path = "/users"
    primary_keys = ["id"]
    replication_key = None
    # Optionally, you may also use `schema_filepath` in place of `schema`:
    # schema_filepath = SCHEMAS_DIR / "users.json"
    schema = th.PropertiesList(
        th.Property("User",
                        th.ObjectType(
                            th.Property("Id", th.IntegerType),
                            th.Property("FullName", th.StringType),
                            th.Property("ImageUrl", th.StringType),
                            th.Property("Username", th.StringType),
                            th.Property("connection", th.StringType),
                            th.Property("IsAgent", th.BooleanType),
                            th.Property("SubscriptionPeriodEnd", th.DateTimeType),
                            th.Property("AgentSubscriptionIsActive", th.BooleanType),
                            th.Property("Title", th.StringType),
                            th.Property("Phone", th.StringType),
                            th.Property("Url", th.StringType),
                            th.Property("CompanyLogoUrl", th.StringType),
                        )
                    ),
        th.Property("AccessToken", th.StringType),
        th.Property("PublicKey", th.StringType),
        th.Property("AgentSubscriptionLevel", th.IntegerType),
        th.Property("IsActive", th.BooleanType),
        th.Property("CreatedOn", th.DateTimeType),
    ).to_dict()
